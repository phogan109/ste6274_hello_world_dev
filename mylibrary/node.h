#ifndef NODE_H
#define NODE_H

#include <memory>

namespace mylib {

    struct Node {

    using value_type = int;

    public:
        Node();
        Node(const value_type);
        Node(value_type, std::shared_ptr<Node>);
        ~Node();
        void setValue(int i);
        void setNext(std::shared_ptr<Node>);
        int getValue();
        std::shared_ptr<Node> getNext();

    private:
        std::shared_ptr<Node> _next;
        int _value;
    };

}

#endif // NODE_H

