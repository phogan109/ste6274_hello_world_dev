#include "node.h"
#include "list.h"
#include <iostream>
#include <memory>



namespace mylib {

    // CONSTRUCTOR

  List::List(std::initializer_list<int> list)
      : _size(list.size()), _root(nullptr), _last(nullptr)
  {
      //SET UP EXTRA VARIABLES NEEDED

      Node _next = Node();

      //LOOP OVER ELEMENTS PROVIDED IN CONSTRUCTOR, IN REVERSE ORDER

      for (std::initializer_list<int>::iterator it=list.end() - 1; it!=list.begin() -1; --it)
      {
        //MAKE NEW NODE; SET IT AS THE _root

        _next = Node(*it);

        //SET THE NEXT NODE OF THE ROOT

        _next.setNext(_root);

        //SET THE NEXT NODE POINTER VARIABLE EQUAL TO THE ROOT

        _root = std::make_shared<Node>(_next);

        /*std::cout << " inside constructor- element: " << *it << std::endl;
          std::cout << "                  root value: " << _root.getValue() << std::endl;
          std::cout << "                root pointer: " << _root.getNext() << std::endl; */


        //IF THE ITEM IS THE FIRST ITEM, SET _first

        if (it == list.begin())
        {
             //std::cout << "In First Node IF " << std::endl;
            _root = std::make_shared<Node>(_next);
        }
        //IF THE ITEM IS THE LAST ITEM, SET _last
        else if (it == list.end() - 1)
        {
            //std::cout << "In Last Node IF " << std::endl;
            _last = std::make_shared<Node>(_next);
        }


      }

  }


  // DESTRUCTOR
  List::~List()
  {

  }

  //SIZE()
  std::size_t List::size() const
  {
    return _size;
  }


  //BEGIN()
  std::shared_ptr<Node> List::begin()
  {
    return this->_root;
  }

  //END()
  std::shared_ptr<Node> List::end()
  {
    return this->_last;
  }

  //FRONT()
  Node List::front()
  {
    return *_root;
  }

  //BACK()
  Node List::back()
  {
    return *_last;
  }

  //PUSH_BACK()
  void List::push_back(const int i)
  {

      Node newBack;

      newBack = Node(i);

      this->back().setNext(std::make_shared<Node>(newBack));

      _last = std::make_shared<Node>(newBack);

      _size++;
  }

  //PUSH_FRONT()
  void List::push_front(int i)
  {
      Node newFront;

      newFront = Node(i, this->begin());

      _root = std::make_shared<Node>(newFront);

      _size++;
  }

  //PRINTLIST
  void List::printList()
  {
    for(std::shared_ptr<Node> i = this->begin(); i != this->end(); i = (*i).getNext())
    {
        std::cout << (*i).getValue() << std::endl;
        std::cout << (*i).getNext() << std::endl;
    }
  }


}
