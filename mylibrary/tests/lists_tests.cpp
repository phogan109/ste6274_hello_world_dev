
#include <gtest/gtest.h>

#include "../list.cpp"
#include "../node.cpp"


#define UNUSED(ARG) (void)ARG

TEST(Container_List, Size_functionality) {

  using namespace mylib;

  List list { 1, 2, 3, 4 };

  EXPECT_EQ( 3, list.size() );

}

