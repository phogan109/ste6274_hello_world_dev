#include "node.h"

namespace mylib {

  using value_type = int;

  Node::Node()
  {

  }

  Node::Node(const value_type i)
      : _value(i) {}

  Node::Node(value_type i, std::shared_ptr<Node> newNode)
      : _next(newNode), _value(i) {}

  Node::~Node()
  {

  }

  void Node::setNext(std::shared_ptr<Node> a)
  {
      _next = a;
  }

  int Node::getValue()
  {
      return _value;
  }

  std::shared_ptr<Node> Node::getNext()
  {
      return _next;
  }
}


