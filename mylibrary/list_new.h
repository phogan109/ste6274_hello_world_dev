#ifndef LIST_H
#define LIST_H

#include <initializer_list>
#include "node.h"
#include <memory>
#include <iterator>

namespace mylib {


 template < class T, int N > class ListIter;
 template < class T, int N > class ListCIter;

  template < class T, int N >
  class List {
  public:

    typedef ListIter<T, N> iterator;
    typedef ListCIter<T, N> const_iterator;
    typedef ptrdiff_t difference_type;
    typedef size_t size_type;
    typedef T value_type;
    typedef T * pointer;
    typedef T & reference;


    List( std::initializer_list<value_type> )
    {


    }

    ~List()
    {
    }

    size_type size() const
    {
        return _size;
    }

    iterator begin()
    {
        return iterator(*this, 0);
    }

    iterator end()
    {
        return iterator(*this, _size);
    }

    T front()
    {
        return *iterator(*this, 0);
    }

    T back()
    {
        return *iterator(*this, _size);
    }

    void push_back(const value_type i)
    {

    }

    void push_front(const value_type i)
    {

    }

  private:
    size_type _size;
    iterator _root;
    iterator _last;




  }; //END class List

} //END namespace mylib


#endif // LIST_H

