#ifndef LIST_H
#define LIST_H

#include <initializer_list>
#include "node.h"
#include <memory>

namespace mylib {

  class List {
  public:
    using size_type = std::size_t  ;
    using value_type = int;

    List( std::initializer_list<value_type> );
    ~List();

    size_type size() const;
    std::shared_ptr<Node> begin();
    std::shared_ptr<Node> end();
    Node front();
    Node back();
    void push_back(const value_type i);
    void push_front(const value_type i);
    void printList();

  private:
    size_type _size;
    std::shared_ptr<Node> _root;
    std::shared_ptr<Node> _last;


  }; //END class List

} //END namespace mylib


#endif // LIST_H

