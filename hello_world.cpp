// mylib
#include "mylibrary/functions.h"
#include "mylibrary/list.h"
#include "mylibrary/node.cpp"

// stl
#include <iostream>
#include <stdexcept>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;

int main(int argc, char** argv) try {
  UNUSED(argc)
  UNUSED(argv)

  /* NODE TEST

  mylib::Node node1(1);
  mylib::Node node2(2);
  mylib::Node * a = &node2;
  int b = 0;

  std::cout << node1.getValue() << std::endl;
  std::cout << node2.getValue() << std::endl;
  node1.setNext(a);
  b = (node1.getNext())->getValue();
  std::cout << b << std::endl;

  //mylib::helloWorld( "... anywho ..." );

   */



  try
  {
    mylib::List list {1,2,3} ;

    std::cout << "Initial List Size: " << list.size() << std::endl;

    std::cout << "Push Front (9): " << std::endl;

    list.push_front(9);

    std::cout << "New _root Value/Next: " << (*(list.begin())).getValue() << " " << (*(list.begin())).getNext() << std::endl;

    std::cout << "New List Size: " << list.size() << std::endl;

    list.push_back(10);

    std::cout << "Push Back (10): " << std::endl;

    std::cout << "New _last Value/Next: " << (*(list.end())).getValue() << " " << (*(list.end())).getNext() << std::endl;

    std::cout << "New List Size: " << list.size() << std::endl;

  //  list.push_back(11);

    std::cout << "PRINT LIST: " <<  std::endl;

    list.printList();


  }
  catch (const std::exception e)
  {
     std::cout << "An exception occurred: " << e.what() << std::endl;
  }


  /*mylib::List::size_type list_size;

  list_size = list.size();

  auto list_size_2 = list.size(); */

  return 0;
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
